=================================
Keycloak Collection Release Notes
=================================

.. contents:: Topics


v1.1.0
======

Release Summary
---------------

Added more roles for the existing modules and improved testing of the collection.

Security Fixes
--------------

- jugasit.keycloak.users role - do not log loop data when it contains sensitive data

New Modules
-----------

- jugasit.keycloak.authentication_info - Query information of Keycloak authentications via Keycloak API
- jugasit.keycloak.client_info - Query information of Keycloak clients via Keycloak API
- jugasit.keycloak.role_info - Query information of Keycloak roles via Keycloak API

New Roles
---------

- jugasit.keycloak.authentications - Configure authentication in Keycloak
- jugasit.keycloak.jugasit.keycloak.clients - Manage clients in Keycloak
- jugasit.keycloak.jugasit.keycloak.roles - Manage roles in Keycloak
- jugasit.keycloak.jugasit.keycloak.user_rolemappings - Manage user role mappings in Keycloak

v1.0.0
======

Release Summary
---------------

Initial release of the collection with modules forked from `community.general` and a few roles included.

New Playbooks
-------------

- jugasit.keycloak.configure - Configure Keycloak

New Modules
-----------

- jugasit.keycloak.authentication - Configure authentication in Keycloak
- jugasit.keycloak.authz_authorization_scope - Allows administration of Keycloak client authorization scopes via Keycloak API
- jugasit.keycloak.client - Allows administration of Keycloak clients via Keycloak API
- jugasit.keycloak.client_rolemapping - Allows administration of Keycloak client_rolemapping with the Keycloak API
- jugasit.keycloak.clientscope - Allows administration of Keycloak client_scopes via Keycloak API
- jugasit.keycloak.clientscope_type - Set the type of aclientscope in realm or client via Keycloak API
- jugasit.keycloak.clientsecret_info - Retrieve client secret via Keycloak API
- jugasit.keycloak.clientsecret_regenerate - Regenerate Keycloak client secret via Keycloak API
- jugasit.keycloak.group - Allows administration of Keycloak groups via Keycloak API
- jugasit.keycloak.group_info - Query information of Keycloak groups via Keycloak API
- jugasit.keycloak.identity_provider - Allows administration of Keycloak identity providers via Keycloak API
- jugasit.keycloak.realm - Allows administration of Keycloak realm via Keycloak API
- jugasit.keycloak.realm_info - Allows obtaining Keycloak realm public information via Keycloak API
- jugasit.keycloak.role - Allows administration of Keycloak roles via Keycloak API
- jugasit.keycloak.user - Allows administration of Keycloak users via Keycloak API
- jugasit.keycloak.user_federation - Allows administration of Keycloak user federations via Keycloak API
- jugasit.keycloak.user_info - Query information of Keycloak users via Keycloak API
- jugasit.keycloak.user_rolemapping - Allows administration of Keycloak user_rolemapping with the Keycloak API

New Roles
---------

- jugasit.keycloak.groups - Manage groups in Keycloak
- jugasit.keycloak.realms - Manage realms in Keycloak
- jugasit.keycloak.users - Manage users in Keycloak
