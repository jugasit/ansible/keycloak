default: help
help:
	@echo "Please use \`make <target>' where <target> is one of:"
	@echo "  help             to show this message"
	@echo "  test             to run unit tests"
	@echo "  docs             to build the collection documentation"
	@echo "  clean            to remove temporary build files"

test:
	molecule test --all

build:
	ansible-galaxy collection build --force .

install: build
	ansible-galaxy collection install --force *.tar.gz

docs: install
	pip install -r docs/requirements.txt
	mkdir -p docs/roles docs/plugins
	cat ./docs/roles.rst.template > ./docs/roles/index.rst
	echo "# Overview" > docs/README.md
	tail --lines +2 README.md >> docs/README.md
	for role_readme in roles/*/README.md; do \
		ln -f -s ../../$$role_readme ./docs/roles/$$(basename $$(dirname $$role_readme)).md; \
		echo " * :doc:\`$$(basename $$(dirname $$role_readme))\`" >> ./docs/roles/index.rst; \
	done
	antsibull-docs collection --use-current --squash-hierarchy --dest-dir ./docs/plugins jugasit.keycloak
	cat ./docs/plugins.rst.template > ./docs/plugins/index.rst
	for module in plugins/modules/*.py; do \
		echo " * :ref:\`jugasit.keycloak.$$(basename -s .py $$module) <ansible_collections.jugasit.keycloak.$$(basename -s .py $$module)_module>\`" >> ./docs/plugins/index.rst; \
	done
	make -C docs html
	

clean:
	rm -rf docs/_build docs/roles docs/plugins docs/README.md *.tar.gz

.PHONY: help test docs clean build install
