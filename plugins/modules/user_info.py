#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (c) 2023, Christoffer Reijer <christoffer@jugasit.com>
# GNU General Public License v3.0+ (see LICENSES/GPL-3.0-or-later.txt or https://www.gnu.org/licenses/gpl-3.0.txt)
# SPDX-License-Identifier: GPL-3.0-or-later

from __future__ import absolute_import, division, print_function
__metaclass__ = type

DOCUMENTATION = '''
---
module: user_info
short_description: Query information of Keycloak users via Keycloak API
version_added: 1.0.0
description:
    - This module allows you to query information about Keycloak users via the Keycloak REST API.
      It requires access to the REST API via OpenID Connect; the user connecting and the client being
      used must have the requisite access rights. In a default Keycloak installation, admin-cli
      and an admin user would work, as would a separate client definition with the scope tailored
      to your needs and a user having the expected roles.
    - The names of module options are snake_cased versions of the camelCase ones found in the
      Keycloak API and its documentation at U(https://www.keycloak.org/docs-api/8.0/rest-api/index.html).
    - Attributes are multi-valued in the Keycloak API. All attributes are lists of individual values and will
      be returned that way by this module. You may pass single values for attributes when calling the module,
      and this will be translated into a list suitable for the API.
options:
    realm:
        type: str
        default: master
        description:
            - Realm of the user.
    username:
        type: str
        required: true
        description:
            - Username of the user.
extends_documentation_fragment:
    - jugasit.keycloak.keycloak
    - jugasit.keycloak.attributes
    - jugasit.keycloak.attributes.info_module
author:
    - Christoffer Reijer (@ephracis)
'''

EXAMPLES = '''
- name: Get a Keycloak user
  jugasit.keycloak.user_info:
    name: MyCustomUser
    auth_client_id: admin-cli
    auth_keycloak_url: https://auth.example.com/auth
    auth_realm: master
    auth_username: USERNAME
    auth_password: PASSWORD
  delegate_to: localhost
'''

RETURN = '''
msg:
    description: Message as to what action was taken.
    returned: always
    type: str
user_info:
    description:
        - Representation of the user information.
    returned: always
    type: dict
    contains:
        createdTimestamp:
            description: Timestamp when the user was created.
            type: str
            returned: always
            sample: 1683728585110
        disableableCredentialTypes:
            description: List of disabled credential types.
            type: str
            returned: always
            sample: []
        email:
            description: Email for the user.
            type: str
            returned: always
            sample: test1@example.com
        emailVerified:
            description: Whether or not the email for the user has been verified.
            type: bool
            returned: always
            sample: true
        enabled:
            description: Whether or not the user is enabled.
            type: bool
            returned: always
            sample: true
        firstName:
            description: First name of the user.
            type: str
            returned: always
            sample: Alice
        lastName:
            description: Last name of the user.
            type: str
            returned: always
            sample: Userson
        username:
            description: Username of the user.
            type: str
            returned: always
            sample: test1
        groups:
            description: List of groups the user is a member of.
            type: list
            returned: always
            sample: [{"name": "group1"}]
'''

from ansible_collections.jugasit.keycloak.plugins.module_utils.keycloak import KeycloakAPI, camel, \
    keycloak_argument_spec, get_token, KeycloakError
from ansible.module_utils.basic import AnsibleModule


def main():
    """
    Module execution
    :return:
    """
    argument_spec = keycloak_argument_spec()

    meta_args = dict(
        username=dict(type='str', required=True),
        realm=dict(type='str', default='master')
    )

    argument_spec.update(meta_args)

    module = AnsibleModule(argument_spec=argument_spec,
                           supports_check_mode=True,
                           required_one_of=([['token', 'auth_realm', 'auth_username', 'auth_password']]),
                           required_together=([['auth_realm', 'auth_username', 'auth_password']]))

    result = dict(changed=False, msg='', user_info={})

    # Obtain access token, initialize API
    try:
        connection_header = get_token(module.params)
    except KeycloakError as e:
        module.fail_json(msg=str(e))

    kc = KeycloakAPI(module, connection_header)

    # Get user in Keycloak
    realm = module.params.get('realm')
    username = module.params.get('username')
    user = kc.get_user_by_username(username, realm)
    if user is None:
        user = {}
    else:
        # Get extra information about the user
        user['groups'] = sorted(
            kc.get_user_groups(user['id'], realm),
            key=lambda d: d['name']
        )
        user['rolemappings'] = sorted(
            kc.get_realm_user_rolemappings(user['id'], realm),
            key=lambda d: d['name']
        )

    # Return info
    result['user_info'] = user
    result['msg'] = 'Get user info successful for user {username}'.format(username=username)

    module.exit_json(**result)


if __name__ == '__main__':
    main()
