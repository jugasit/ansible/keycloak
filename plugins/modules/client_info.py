#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (c) 2023, Christoffer Reijer <christoffer@jugasit.com>
# GNU General Public License v3.0+ (see LICENSES/GPL-3.0-or-later.txt or https://www.gnu.org/licenses/gpl-3.0.txt)
# SPDX-License-Identifier: GPL-3.0-or-later

from __future__ import absolute_import, division, print_function
__metaclass__ = type

DOCUMENTATION = '''
---
module: client_info
short_description: Query information of Keycloak clients via Keycloak API
version_added: 1.1.0
description:
    - This module allows you to query information about Keycloak clients via the Keycloak REST API.
      It requires access to the REST API via OpenID Connect; the user connecting and the client being
      used must have the requisite access rights. In a default Keycloak installation, admin-cli
      and an admin user would work, as would a separate client definition with the scope tailored
      to your needs and a user having the expected roles.
    - The names of module options are snake_cased versions of the camelCase ones found in the
      Keycloak API and its documentation at U(https://www.keycloak.org/docs-api/8.0/rest-api/index.html).
    - Attributes are multi-valued in the Keycloak API. All attributes are lists of individual values and will
      be returned that way by this module. You may pass single values for attributes when calling the module,
      and this will be translated into a list suitable for the API.
options:
    realm:
        type: str
        default: master
        description:
            - Realm of the client.
    client_id:
        description:
            - Client id of client to fetch information on. This is usually an alphanumeric name chosen by
              you. Either this or I(id) is required. If you specify both, I(id) takes precedence.
              This is 'clientId' in the Keycloak REST API.
        aliases:
            - clientId
        type: str

    id:
        description:
            - Id of client to fetch information on. This is usually an UUID. Either this or I(client_id)
              is required. If you specify both, this takes precedence.
        type: str
extends_documentation_fragment:
    - jugasit.keycloak.keycloak
    - jugasit.keycloak.attributes
    - jugasit.keycloak.attributes.info_module
author:
    - Christoffer Reijer (@ephracis)
'''

EXAMPLES = '''
- name: Get a Keycloak client
  jugasit.keycloak.client_info:
    name: MyCustomClient
    auth_client_id: admin-cli
    auth_keycloak_url: https://auth.example.com/auth
    auth_realm: master
    auth_username: USERNAME
    auth_password: PASSWORD
  delegate_to: localhost
'''

RETURN = '''
msg:
    description: Message as to what action was taken.
    returned: always
    type: str
client_info:
    description:
        - Representation of the client information.
    returned: always
    type: dict
    contains:
        createdTimestamp:
            description: Timestamp when the client was created.
            type: str
            returned: always
            sample: 1683728585110
        disableableCredentialTypes:
            description: List of disabled credential types.
            type: list
            returned: always
            sample: []
        id:
            description: ID of the client.
            type: str
            returned: always
            sample: test1
'''

from ansible_collections.jugasit.keycloak.plugins.module_utils.keycloak import KeycloakAPI, camel, \
    keycloak_argument_spec, get_token, KeycloakError
from ansible.module_utils.basic import AnsibleModule


def main():
    """
    Module execution
    :return:
    """
    argument_spec = keycloak_argument_spec()

    meta_args = dict(
        id=dict(type='str'),
        client_id=dict(type='str', aliases=['clientId']),
        realm=dict(type='str', default='master')
    )

    argument_spec.update(meta_args)

    module = AnsibleModule(argument_spec=argument_spec,
                           supports_check_mode=True,
                           required_one_of=([['client_id', 'id'],
                                             ['token', 'auth_realm', 'auth_username', 'auth_password']]),
                           required_together=([['auth_realm', 'auth_username', 'auth_password']]))

    result = dict(changed=False, msg='', client_info={})

    # Obtain access token, initialize API
    try:
        connection_header = get_token(module.params)
    except KeycloakError as e:
        module.fail_json(msg=str(e))

    kc = KeycloakAPI(module, connection_header)

    # Get client in Keycloak
    realm = module.params.get('realm')
    cid = module.params.get('id')

    if cid is None:
        client = kc.get_client_by_clientid(module.params.get('client_id'), realm=realm)
        if client is not None:
            cid = client['id']
    else:
        client = kc.get_client_by_id(cid, realm=realm)

    if client is None:
        client = {}

    # Return info
    result['client_info'] = client
    result['msg'] = 'Get client info successful for client {cid}'.format(cid=cid)

    module.exit_json(**result)


if __name__ == '__main__':
    main()
