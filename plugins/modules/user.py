#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (c) 2023, Christoffer Reijer <christoffer@jugasit.com>
# GNU General Public License v3.0+ (see LICENSES/GPL-3.0-or-later.txt or https://www.gnu.org/licenses/gpl-3.0.txt)
# SPDX-License-Identifier: GPL-3.0-or-later

from __future__ import absolute_import, division, print_function
__metaclass__ = type

DOCUMENTATION = '''
---
module: user
short_description: Allows administration of Keycloak users via Keycloak API
version_added: 1.0.0
description:
    - This module allows you to add, remove or modify Keycloak users via the Keycloak REST API.
      It requires access to the REST API via OpenID Connect; the user connecting and the client being
      used must have the requisite access rights. In a default Keycloak installation, admin-cli
      and an admin user would work, as would a separate client definition with the scope tailored
      to your needs and a user having the expected roles.
    - The names of module options are snake_cased versions of the camelCase ones found in the
      Keycloak API and its documentation at U(https://www.keycloak.org/docs-api/8.0/rest-api/index.html).
    - Attributes are multi-valued in the Keycloak API. All attributes are lists of individual values and will
      be returned that way by this module. You may pass single values for attributes when calling the module,
      and this will be translated into a list suitable for the API.
options:
    state:
        description:
            - State of the user.
            - On C(present), the user will be created if it does not yet exist, or updated with the parameters you provide.
            - On C(absent), the user will be removed if it exists.
        default: 'present'
        type: str
        choices:
            - present
            - absent
    realm:
        type: str
        default: master
        description:
            - Realm of the user.
    username:
        type: str
        required: true
        description:
            - Username of the user.
            - This parameter is required.
    password:
        type: str
        description:
            - Password to set for the user.
            - If user already exists, you must set C(update_secrets) to C(true) to change password.
    update_secrets:
        type: bool
        default: false
        description:
            - Whether or not to update the password if the user exists.
    groups:
        type: list
        description:
            - List of groups the user should be a member of.
        elements: str
    append_groups:
      type: bool
      default: true
      description:
        - If C(yes), then add the user to the groups specified in C(groups).
        - If C(no), then the module only adds the user to the groups specified in
          C(groups), removing them from all other groups.
    email:
        type: str
        description:
            - Email of the user.
    email_verified:
        type: bool
        default: true
        description:
            - Whether or not the email has been verified for the user.
    enabled:
        type: bool
        default: true
        description:
            - Whether or not the user is enabled.
    first_name:
        type: str
        description:
            - First name of the user.
    last_name:
        type: str
        description:
            - Last name of the user.
    attributes:
        type: dict
        description:
            - A dict of key/value pairs to set as custom attributes for the user.
            - Values may be single values (e.g. a string) or a list of strings.
extends_documentation_fragment:
    - jugasit.keycloak.keycloak
    - jugasit.keycloak.attributes
author:
    - Christoffer Reijer (@ephracis)
'''

EXAMPLES = '''
- name: Create a Keycloak realm user, authentication with credentials
  jugasit.keycloak.user:
    name: my-new-kc-user
    realm: MyCustomRealm
    state: present
    auth_client_id: admin-cli
    auth_keycloak_url: https://auth.example.com/auth
    auth_realm: master
    auth_username: USERNAME
    auth_password: PASSWORD
  delegate_to: localhost
- name: Create a Keycloak realm user, authentication with token
  jugasit.keycloak.user:
    name: my-new-kc-user
    realm: MyCustomRealm
    state: present
    auth_client_id: admin-cli
    auth_keycloak_url: https://auth.example.com/auth
    token: TOKEN
  delegate_to: localhost
- name: Delete a Keycloak user
  jugasit.keycloak.user:
    name: my-user-for-deletion
    state: absent
    auth_client_id: admin-cli
    auth_keycloak_url: https://auth.example.com/auth
    auth_realm: master
    auth_username: USERNAME
    auth_password: PASSWORD
  delegate_to: localhost
- name: Create a keycloak user with some custom attributes
  jugasit.keycloak.user:
    auth_client_id: admin-cli
    auth_keycloak_url: https://auth.example.com/auth
    auth_realm: master
    auth_username: USERNAME
    auth_password: PASSWORD
    name: my-new-user
    attributes:
        attrib1: value1
        attrib2: value2
        attrib3:
            - with
            - numerous
            - individual
            - list
            - items
  delegate_to: localhost
'''

RETURN = '''
msg:
    description: Message as to what action was taken.
    returned: always
    type: str
    sample: "User test1 has been updated"
proposed:
    description: Representation of proposed user.
    returned: always
    type: dict
    sample: {
        "lastName": "One"
    }
existing:
    description: Representation of existing user (sample is truncated).
    returned: always
    type: dict
    sample: {
        "attributes": {},
        "id": "561703dd-0f38-45ff-9a5a-0c978f794547",
        "username": "user1",
        "email": "user1@example.com",
        "firstName": "User",
        "lastName": "111"
    }
end_state:
    description: Representation of user after module execution (sample is truncated).
    returned: on success
    type: dict
    sample: {
        "attributes": {},
        "id": "561703dd-0f38-45ff-9a5a-0c978f794547",
        "username": "user1",
        "email": "user1@example.com",
        "firstName": "User",
        "lastName": "One"
    }
'''

from ansible_collections.jugasit.keycloak.plugins.module_utils.keycloak import KeycloakAPI, camel, \
    keycloak_argument_spec, get_token, KeycloakError
from ansible.module_utils.basic import AnsibleModule


def main():
    """
    Module execution
    :return:
    """
    argument_spec = keycloak_argument_spec()

    meta_args = dict(
        state=dict(type='str', default='present', choices=['present', 'absent']),
        username=dict(type='str', required=True),
        password=dict(type='str', required=False, no_log=True),
        update_secrets=dict(type='bool', default=False, no_log=False),
        groups=dict(type='list', elements='str', required=False),
        append_groups=dict(type="bool", default=True),
        email=dict(type='str', required=False),
        email_verified=dict(type='bool', default=True),
        enabled=dict(type='bool', default=True),
        first_name=dict(type='str', required=False),
        last_name=dict(type='str', required=False),
        realm=dict(type='str', default='master'),
        attributes=dict(type='dict'),
    )

    argument_spec.update(meta_args)

    module = AnsibleModule(argument_spec=argument_spec,
                           supports_check_mode=True,
                           required_one_of=([['token', 'auth_realm', 'auth_username', 'auth_password']]),
                           required_together=([['auth_realm', 'auth_username', 'auth_password']]))

    result = dict(changed=False, msg='', diff={}, proposed={}, existing={}, end_state={})

    # Obtain access token, initialize API
    try:
        connection_header = get_token(module.params)
    except KeycloakError as e:
        module.fail_json(msg=str(e))

    kc = KeycloakAPI(module, connection_header)

    realm = module.params.get('realm')
    username = module.params.get('username')
    password = module.params.get('password')
    groups = module.params.get('groups')
    append_groups = module.params.get("append_groups")
    state = module.params.get('state')

    # attributes in Keycloak have their values returned as lists
    # via the API. attributes is a dict, so we'll transparently convert
    # the values to lists.
    if module.params.get('attributes') is not None:
        for key, val in module.params['attributes'].items():
            module.params['attributes'][key] = [val] if not isinstance(val, list) else val

    # Filter and map the parameters names that apply to the user
    user_params = [x for x in module.params
                   if x not in list(keycloak_argument_spec().keys()) +
                   ['state', 'realm', 'append_groups', 'password', 'update_secrets'] and
                   module.params.get(x) is not None]

    # See if it already exists in Keycloak
    before_user = kc.get_user_by_username(username, realm)

    if before_user is None:
        before_user = {}

    # Build a proposed changeset from parameters given to this module
    changeset = {}

    for param in user_params:
        new_param_value = module.params.get(param)
        old_value = before_user[camel(param)] if camel(param) in before_user else None
        if new_param_value != old_value:
            changeset[camel(param)] = new_param_value

    # Prepare the desired values using the existing values (non-existence results in a dict that is save to use as a basis)
    desired_user = before_user.copy()
    desired_user.update(changeset)

    result['proposed'] = changeset
    result['existing'] = before_user

    # Cater for when it doesn't exist (an empty dict)
    if not before_user:
        if state == 'absent':
            # Do nothing and exit
            if module._diff:
                result['diff'] = dict(before='', after='')
            result['changed'] = False
            result['end_state'] = {}
            result['msg'] = 'User does not exist, doing nothing.'
            module.exit_json(**result)

        # Process a creation
        result['changed'] = True

        if username is None:
            module.fail_json(msg='User name must be specified when creating a new user')

        if module._diff:
            result['diff'] = dict(before='', after=desired_user)

        if module.check_mode:
            module.exit_json(**result)

        # set password
        if password:
            desired_user["credentials"] = [{
                "type": "password",
                "value": password,
                "temporary": False
            }]

        # create it
        kc.create_user(desired_user, realm)
        after_user = kc.get_user_by_username(username, realm)

        result['end_state'] = after_user

        result['msg'] = 'User {username} has been created'.format(username=username)
        module.exit_json(**result)

    else:
        if state == 'present':
            # Process an update

            # get groups in order to detect changes
            current_groups = kc.get_user_groups(before_user['id'], realm)
            if len(current_groups) > 0:

                # if appending groups, remove non-desired group from existing list
                if append_groups:
                    before_user['groups'] = []
                    for g in desired_user.get('groups', []):
                        if g in desired_user['groups']:
                            before_user['groups'].append(g)
                else:
                    before_user['groups'] = [g['name'] for g in current_groups]

            # no changes
            if desired_user == before_user:
                if password and module.params.get('update_secrets'):
                    module.warn('The password contains encrypted data and may inaccurately report task is changed.')
                else:
                    result['changed'] = False
                    result['msg'] = "No changes required to user {username}.".format(username=username)
                    result['end_state'] = desired_user
                    module.exit_json(**result)

            # doing an update
            result['changed'] = True

            if module._diff:
                result['diff'] = dict(before=before_user, after=desired_user)

            if module.check_mode:
                module.exit_json(**result)

            # assign groups
            if groups:
                kc.update_user_groups(before_user['id'], groups, append_groups, realm)

            # set password
            if password and module.params.get('update_secrets'):
                desired_user["credentials"] = [{
                    "type": "password",
                    "value": password,
                    "temporary": False
                }]

            # do the update
            kc.update_user(desired_user, realm)
            after_user = kc.get_user_by_username(username, realm)

            result['end_state'] = after_user

            result['msg'] = "User {username} has been updated".format(username=username)
            module.exit_json(**result)

        else:
            # Process a deletion (because state was not 'present')
            result['changed'] = True

            if module._diff:
                result['diff'] = dict(before=before_user, after='')

            if module.check_mode:
                module.exit_json(**result)

            # delete it
            kc.delete_user(before_user['id'], realm)

            result['end_state'] = {}

            result['msg'] = "User {username} has been deleted".format(username=username)

    module.exit_json(**result)


if __name__ == '__main__':
    main()
