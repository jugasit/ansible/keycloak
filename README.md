# Ansible Collection for Keycloak / Red Hat SSO

This Ansible collection allows for easy interaction with Keycloak or Red Hat SSO via Ansible roles and modules.

See the [documentation](http://jugasit.gitlab.io/ansible/keycloak) for more information.

## Support

## Keycloak

This collection has been tested against the following versions but may work well with other versions as well:

- Keycloak 18.0
- Keycloak 21.1

## Python and Ansible

This collection has been tested against the following combinations but may work with other versions as well:

- Ansible 2.12 and Python 3.8
- Ansible 2.12 and Python 3.9
- Ansible 2.13 and Python 3.8
- Ansible 2.13 and Python 3.9
- Ansible 2.14 and Python 3.9
- Ansible 2.14 and Python 3.11

## Installing this collection

You can install the collection with the Ansible Galaxy CLI:

```console
ansible-galaxy collection install jugasit.keycloak
```

You can also include it in a `requirements.yml` file and install it with `ansible-galaxy collection install -r requirements.yml`, using the format:

```yaml
---
collections:
  - name: jugasit.keycloak
    # If you need a specific version of the collection, you can specify like this:
    # version: ...
```

## Using the Collection

### Migrating from community.general

This collection is forked from `community.general` but with only the Keycloak specific modules kept.

There are however a few breaking changes in this collection:

- The alias `username` for `auth_username` was removed.
- The alias `password` for `auth_password` was removed.

So instead of this:

```yaml
- name: Create realm
  community.general.keycloak_realm:
    auth_keycloak_url: https://keycloak:8443
    auth_realm: master
    username: admin
    password: changeme
    id: myrealm
    realm: myrealm
```

You need to do this:

```yaml
- name: Create realm
  jugasit.keycloak.realm:
    auth_keycloak_url: https://keycloak:8443
    auth_realm: master
    auth_username: admin
    auth_password: changeme
    id: myrealm
    realm: myrealm
```

### Playbook

The collection includes a playbook named `configure.yml`. You can run this playbook directly,
or import it from your own playbook.

This playbook will run all the roles, and is a great starting point for configuring Keycloak
using Ansible.

#### Provide variables

To send variables to this playbook, put them into YAML files inside a directory and point to it
using either the environment variable `KEYCLOAK_CONFIGS_DIR` or the variable `keycloak_configs_dir`.

The variables for each role is specified as a list with the name `keycloak_<module in plural>`, and
each item supports the same attributes as the module. For example:

```yaml
keycloak_realms:
  - id: myrealm
    realm: myrealm
    description: My realm.

keycloak_groups:
  - name: mygroup
    realm: myrealm

keycloak_users:
  - username: user1
    first_name: User
    last_name: One
    groups:
      - mygroup
```

#### Run playbook from CLI

To run it directly:

```shell
ansible-playbook -e keycloak_configs_dir=$PWD/configs jugasit.keycloak.configure
```

#### Import playbook

The playbook can also be imported into an existing playbook. This is useful if you want to
run the playbook from AAP with your own data, for example. It also allows you to include this
collection into a larger flow with multiple collections and/or your own roles and tasks.

To import it from your own playbook:

```yaml
# my_playbook.yml
- import_playbook: jugasit.keycloak.configure
  vars:
    keycloak_configs_dir: "{{ lookup('env', 'PWD') }}/configs"
```

Then run your own playbook like so:

```shell
ansible-playbook my_playbook.yml
```

#### Tags

Control which role to run by specifying tags to either include or exclude. The tags use the same
names as the roles. For example `realms`, `authentications`, `clients`, `users`, etc.

For example:

```shell
ansible-playbook ... --skip-tags users,groups,roles jugasit.keycloak.configure
```
