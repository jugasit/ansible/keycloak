# jugasit.keycloak.authentications

## Description

This role creates, manages and deletes authentications in Keycloak.

## Requirements

- Ansible 2.10 or later.
- `community.general`

## Variables

### Authentication

|Variable Name|Default Value|Required|Description|
|-----|-----|-----|-----|
|`keycloak_auth_url`||no|URL to the Keycloak or Red Hat SSO instance.|
|`keycloak_auth_username`||no|Username to authenticate for API access with.|
|`keycloak_auth_password`||no|Password to authenticate for API access with.|
|`keycloak_auth_realm`||no|Keycloak realm name to authenticate to for API access.|
|`keycloak_auth_client_id`||no|OpenID Connect `client_id` to authenticate to the API with.|
|`keycloak_auth_client_secret`||no|authentication Secret to use in conjunction with `keycloak_auth_client_id` (if required).|
|`keycloak_auth_token`||no|Authentication token to authenticate for API access with.|
|`keycloak_validate_certs`|`true`|no|Verify TLS certificates (do not disable this in production).|

### Enforcing defaults

The following variables compliment each other.

If both variables are not set, enforcing default values is not done.

Enabling these variables enforce default values on options that are optional in the controller API.
This should be enabled to enforce configuration and prevent configuration drift. It is recomended to be enabled, however it is not enforced by default.

Enabling this will enforce configurtion without specifying every option in the configuration files.

`keycloak_authentications_enforce_defaults` defaults to the value of `keycloak_enforce_defaults` if it is not explicitly called.
This allows for enforced defaults to be toggled for the entire suite of controller configuration roles with a single variable, or for the user to selectively use it.

|Variable Name|Default Value|Required|Description|
|-----|-----|-----|-----|
|`keycloak_authentications_enforce_defaults`|`False`|no|Whether or not to enforce default option values on only this role|
|`keycloak_enforce_defaults`|`False`|no|This variable enables enforced default values as well, but is shared across multiple roles, see above.|

### Secure Logging Variables

The following variables compliment each other.

If both variables are not set, secure logging defaults to false.
The role defaults to `false` as normally the task does not include sensitive information.

`keycloak_authentications_secure_logging` defaults to the value of `keycloak_secure_logging` if it is not explicitly called.
This allows for secure logging to be toggled for the entire suite of configuration roles with a single variable, or for the user to selectively use it.

|Variable Name|Default Value|Required|Description|
|-----|-----|-----|-----|
|`keycloak_authentications_secure_logging`|`False`|no|Whether or not to include the role tasks in the log.  Set this value to `True` if you will be providing your sensitive values from elsewhere.|
|`keycloak_secure_logging`|`False`|no|This variable enables secure logging as well, but is shared across multiple roles, see above.|

### Data structure

|Variable Name|Default Value|Required|Description|
|-----|-----|-----|-----|
|`keycloak_authentications`|`[]`|no|List of authentications to manage. Each item supports the same parameters as the module.|

## Example Playbook

```yaml
---
- name: Create authentications
  hosts: localhost
  connection: local
  gather_facts: false
  vars:
    # Auth variables are omitted, as they should come from vault,
    # credential or environment variables.
    keycloak_authentications:
      - alias: test
        description: Description of this wonderful authentication
  roles:
    - jugasit.keycloak.authentications
```
