# jugasit.keycloak.realms

## Description

This role creates, manages and deletes realms in Keycloak.

## Requirements

- Ansible 2.10 or later.
- `community.general`

## Variables

### Authentication

|Variable Name|Default Value|Required|Description|
|-----|-----|-----|-----|
|`keycloak_auth_url`||no|URL to the Keycloak or Red Hat SSO instance.|
|`keycloak_auth_username`||no|Username to authenticate for API access with.|
|`keycloak_auth_password`||no|Password to authenticate for API access with.|
|`keycloak_auth_realm`||no|Keycloak realm name to authenticate to for API access.|
|`keycloak_auth_client_id`||no|OpenID Connect `client_id` to authenticate to the API with.|
|`keycloak_auth_client_secret`||no|Client Secret to use in conjunction with `keycloak_auth_client_id` (if required).|
|`keycloak_auth_token`||no|Authentication token to authenticate for API access with.|
|`keycloak_validate_certs`|`true`|no|Verify TLS certificates (do not disable this in production).|

### Secure Logging Variables

The following variables compliment each other.

If both variables are not set, secure logging defaults to false.
The role defaults to `false` as normally the task does not include sensitive information.

`keycloak_realms_secure_logging` defaults to the value of `keycloak_secure_logging` if it is not explicitly called.
This allows for secure logging to be toggled for the entire suite of configuration roles with a single variable, or for the user to selectively use it.

|Variable Name|Default Value|Required|Description|
|-----|-----|-----|-----|
|`keycloak_realms_secure_logging`|`False`|no|Whether or not to include the role tasks in the log.  Set this value to `True` if you will be providing your sensitive values from elsewhere.|
|`keycloak_secure_logging`|`False`|no|This variable enables secure logging as well, but is shared across multiple roles, see above.|

### Data structure

|Variable Name|Default Value|Required|Description|
|-----|-----|-----|-----|
|`keycloak_realms`|`[]`|no|List of realms to manage. Each item supports the same parameters as the module.|

## Example Playbook

```yaml
---
- name: Create realms
  hosts: localhost
  connection: local
  gather_facts: false
  vars:
    # Auth variables are omitted, as they should come from vault,
    # credential or environment variables.
    keycloak_realms:
      - id: realm1
        name: realm1
  roles:
    - jugasit.keycloak.realms
```
