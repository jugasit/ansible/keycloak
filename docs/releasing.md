# Releasing

## Create pull request

Create a pull request with the following changes:

- Bump version in `galaxy.yml`
- Create `changelogs/fragments/summary.yml` with a `release_summary: ...`.
- Update changelog by running `antsibull-changelog release`

## Create tag

After merging the pull request, create a tag:

- Go to [Repository > Tags on GitLab](https://gitlab.com/jugasit/ansible/keycloak/-/tags).
- Click on **New tag**.
- Fill out **Tag name** with the new version. For example `1.2.3`.
- Set **Create from** to `main`.
- Fill out **Message** with `Version 1.2.3`.
- Click on **Create tag**.

## Bump version to dev

Create a pull request with the version changed in `galaxy.yml` to the next version with the `-dev`
suffix. For example after `1.0.0` the version should be bumped to `1.1.0-dev`.
