Welcome to Ansible Keycloak Collection
======================================

.. toctree::
   :maxdepth: 2
   :caption: User documentation

   README
   Changelog <CHANGELOG>
   playbooks/index
   roles/index
   plugins/index

.. toctree::
   :maxdepth: 2
   :caption: Developer documentation

   contributing
   testing
   releasing
