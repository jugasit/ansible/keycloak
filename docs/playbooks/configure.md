# jugasit.keycloak.configure

Use this playbook for a simplified way of configuring Keycloak or Red Hat SSO.

This playbook will run all the roles, and is a great starting point for configuring Keycloak
using Ansible.

## Usage

You can run this playbook directly, or import it from your own playbook.

To send variables to this playbook, put them into YAML files inside a directory and point to it
using either the environment variable `KEYCLOAK_CONFIGS_DIR` or the variable `keycloak_configs_dir`.

The variables for each role is specified as a list with the name `keycloak_<module in plural>`, and
each item supports the same attributes as the module. For example:

```yaml
keycloak_realms:
  - id: myrealm
    realm: myrealm
    description: My realm.
```

### Run playbook from CLI

To run it directly:

```shell
ansible-playbook -e keycloak_configs_dir=$PWD/configs jugasit.keycloak.configure
```

### Import playbook

The playbook can also be imported into an existing playbook. This is useful if you want to
run the playbook from AAP with your own data, for example. It also allows you to include this
collection into a larger flow with multiple collections and/or your own roles and tasks.

To import it from your own playbook:

```yaml
# my_playbook.yml
- import_playbook: jugasit.keycloak.configure.yml
  vars:
    keycloak_configs_dir: "{{ lookup('env', 'PWD') }}/configs"
```

Then run your own playbook like so:

```shell
ansible-playbook my_playbook.yml
```

### Tags

Control which role to run by specifying tags to either include or exclude. The tags use the same
names as the roles. For example `realms`, `users`.
