# Testing

## Requirements

To test this collection during development, you must have the following installed:

- Podman
- Ansible Molecule
- Ansible Lint

## Set up Keycloak

To have something to test against, you can set up Keycloak inside a container:

```shell
podman run -p 8080:8080 -e KEYCLOAK_ADMIN=admin -e KEYCLOAK_ADMIN_PASSWORD=admin quay.io/keycloak/keycloak:21.1.1 start-dev
```

To ensure that Ansible Molecule can find Keycloak:

```shell
export KEYCLOAK_URL=http://localhost:8080/
export KEYCLOAK_USERNAME=admin
export KEYCLOAK_PASSWORD=admin
```

## Running the tests

```
molecule test
```
